from os.path import abspath,dirname,join

PROJECT_DIR = abspath(dirname(__file__))
PICTURES_DIR= join(PROJECT_DIR,'pictures/')
PLUGIN_DIR=join(PROJECT_DIR,'plugin/')

PATH_TO_FIREFOX=join(PROJECT_DIR,"profiles/")
PATH_TO_PICKLE=join(PROJECT_DIR,"pickelickAndColegram/")

SQLALCHEMY_DATABASE_URI="sqlite:///" + join(PROJECT_DIR,'storage.db')

LOGS_URI=join(PROJECT_DIR,'logs_working.log')
