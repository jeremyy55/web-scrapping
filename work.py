from time import sleep
from threading import Thread
import logging
from importlib import reload # for logging in anaconda , which cause problems with root logger

from model import Worker, Trip
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, scoped_session
import config

from website.skyscanner import skyscanner
from website.kayak import kayak
from website.kiwi import kiwi
import random
from website.proxy import get_proxy_from_multiple_country,get_proxy_from_one_country,tor_proxy

from utils.strDate import StrDate
import argparse
from pyvirtualdisplay import Display

import datetime



    
if __name__ == '__main__':
    #
    #define logger
    #
    reload(logging) # needed for creating the log file in anaconda. In other case, It use the anaconda root file
    logging.basicConfig(format='%(asctime)s# %(levelname)s: %(message)s',
                        datefmt='%m/%d/%Y %H:%M:%S',
                        filename=config.LOGS_URI,
                        level=logging.DEBUG)
    logging.info("\n#\n#New execution is running\n#")

    #
    # Argument Parser
    # 
    parser = argparse.ArgumentParser()
    parser.add_argument('-v','--xvfb',help="Open the windows inside a Xvfb(X virtual framebuffer",action='store_true')
    parser.add_argument('task',help='type the website that will be visited during this checking routine',choices=['skyscanner','kayak','kiwi'])
    parser.add_argument('-r','--refresh',help='Activate the refresh mode, and refresh the page for creating logs a second time',action='store_true')
    parser.add_argument('-f','--firefoxProfiles',help='Use firefox profiles created for the experiences. Select it in a random way',action='store_true')

    proxy_group=parser.add_mutually_exclusive_group()
    proxy_group.add_argument('-p','--proxyFromSameCountry',help="Allow you to use a proxy server related to the departure country",action='store_true')
    proxy_group.add_argument('-m','--proxyFromMultipleCountry',help='Allow you to use a proxy server relative to different country',action='store_true')
    proxy_group.add_argument('-t','--proxyByUsingTor',help="Allow you to use tor as a proxy server and to compare it with a random country",action='store_true')
    
    args=parser.parse_args()
    logging.info("args:\n{}".format(args))
    
    if args.xvfb:
        print('xvfb')
        display=Display(visible=0, size=(1920,1080))
        display.start()

    if args.task=='skyscanner':
        func=skyscanner
    elif args.task=='kayak':
        func=kayak
    elif args.task =='kiwi':
        func=kiwi

    if args.refresh:
        refresh=True
    else:
        refresh=False
    
    #
    # Prepare Input
    #
    if args.firefoxProfiles:
        firefoxProfiles_dict={
            "fn8q3j8z.plasmaScreenManufacturer":('Barcelone','Paris'),
            "qifnlo0g.oldBambooHotelCaretaker":('Nantes','Manchester'),
            "9oosrpmt.cosyChaletLogger":('Lyon','Madrid'),
            "k2w1l4bf.RaijinDrummer":('Liverpool','Paris'),
            "lrjpwbrn.myThesisJudgingPanel":('Madrid','Londres')
        }
        firefoxProfile=random.choice(list(firefoxProfiles_dict.keys()))
        
        logging.info(f'firefox profiles:\n{firefoxProfile}')
        departure,arrival=firefoxProfiles_dict[firefoxProfile]
    else:
    
        cities=['Barcelone','Madrid',#Spain
        'Paris','Nantes','Lyon',#France
        'Londres','Manchester','Liverpool'#United Kingdom
        ]
        
        departure=random.choice(cities)
        cities.remove(departure)
        arrival = random.choice(cities)
    countries=['Spain','France','United Kingdom']
    begin=random.randint(1,23)# limit are include
    end=begin+7
    begin=StrDate(begin)
    end=StrDate(end)
    kwargs={'departure':departure,'arrival':arrival,'begin':begin,'end':end,'refresh':refresh}
    logging.info(f'kwargs input for worker execution:\n{kwargs}')

    trip=Trip(city_from=departure,city_to=arrival,date_from=begin.to_date(),date_to=end.to_date())
    engine=create_engine(config.SQLALCHEMY_DATABASE_URI)
    session_factory=sessionmaker(bind=engine)
    Session=scoped_session(session_factory)
    session=Session()
    session.add(trip)
    session.commit()
    trip_id=trip.id # to use after with the worker
    Session.remove()

    

    #
    #build the worker
    #
    workers=[]
    
    if args.proxyFromSameCountry:
        matching_dict={
            'Barcelone':'Spain',
            'Madrid':'Spain',
            'Paris':'France',
            'Nantes':'France',
            'Lyon':'France',
            'Londres':'United Kingdom',
            'Manchester':'United Kingdom',
            'Liverpool':'United Kingdom'
        }

        country_name=matching_dict[departure]
        countries.remove(country_name)
        countries.insert(0,country_name)

        try:
            proxies=get_proxy_from_one_country(country_name)
        except Exception as e:
            logging.exception('There is an error with the proxy')
            raise
        logging.info('result for the server:\n{}'.format(proxies)) 

        try:
            if args.firefoxProfiles:
                firefoxProfiles=[]
                for _ in range(len(proxies)):
                    firefoxProfiles.append(None)
                firefoxProfiles[1]=firefoxProfile

                for proxy,fprofile in zip(proxies,firefoxProfiles):
                    workers.append(Worker(trip_id=trip_id,proxy=proxy,profile=fprofile))
                    sleep(2)
            else:
                for proxy in proxies:
                    workers.append(Worker(trip_id=trip_id,proxy=proxy)) #with proxy
                    sleep(2)

        except:
            logging.exception('error while creating worker\n')
            raise
    
    elif args.proxyFromMultipleCountry or args.proxyByUsingTor:
        tor_worker=[]
        if args.proxyFromMultipleCountry:
            
            random.shuffle(countries)
            kw_countries={}
            for country in countries:
                kw_countries[country]=3
            country_min=2
        elif args.proxyByUsingTor:
            countries=['Spain','France','United Kingdom']
            random.shuffle(countries)
            kw_countries={}
            selected_countries= countries
            for country in selected_countries:
                kw_countries[country]=3
            country_min=1
            proxy=tor_proxy()
            tor_worker.append(Worker(trip_id=trip_id,proxy=proxy))
        #
        #find a working proxy located in the good country
        #
        try:
            proxies=get_proxy_from_multiple_country(country_min=country_min,**kw_countries)
        except Exception as e:
            logging.exception('There is an error with the proxy')
            raise

        logging.info('result for the server:\n{}'.format(proxies)) 

        try:
            for proxy in proxies:
                workers.append(Worker(trip_id=trip_id,proxy=proxy)) #with proxy
                sleep(2)
            workers=workers[:1]+tor_worker+workers[1:]
                
        except:
            logging.exception('error while creating worker\n')
            raise
    else:
        number_of_worker=2
        for i in range(number_of_worker):
            workers.append(Worker(trip_id=trip_id))
            sleep(2)


    try:
        #get the threads
        threads=[]
        for worker in workers:
            kwargs['worker']=worker
            #threads.append(Thread(target=skyscanner,kwargs=kwargs))
            #
            # focus on skyscanner
            #
            print('driver for threads:',kwargs['worker'].driver)
            threads.append(Thread(target=func,kwargs=dict(kwargs) ))

            sleep(2)
        print('start threads!')
        for thread in threads:
            thread.start()
        for thread in threads:
            thread.join()

        print('final wait')
        sleep(20)
        print('fin')        
    except:
        logging.exception("There is an error in threads\n")
        #re-raise the exception
        raise
    finally:
        logging.info(f'\n#\n end time: {datetime.datetime.now()}\n#\n')

    if args.xvfb:
        print('xvfb')
        display.stop()