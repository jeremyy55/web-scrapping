from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from os.path import join

import config

def build_driver(profile=None,useragent=None,proxy=None):
    """ 
        build a driver for firefox
        with the possibility to add:
            preferences :
                with fp.set_preference("security.mixed_content.block_active_content", False)    # To allow connections from https to our localhost
            desiredCapabilities : https://github.com/SeleniumHQ/selenium/wiki/DesiredCapabilities
                which could be used for the proxy , for example
            profile: 
            extension such as plugin: 
                fp.add_extension("filepath to extension")
    """
    #
    ##Profile: instance of FirefoxProfile
    #if no profile is given a new one is created automatically
    if profile:
        PATH_TO_PROFILE = join(config.PATH_TO_FIREFOX,profile)
    else:
        PATH_TO_PROFILE=None

    fp = webdriver.FirefoxProfile(PATH_TO_PROFILE)
    
    if useragent:
        fp.set_preference("general.useragent.override",useragent)
    if proxy:
        if proxy.country=='TOR':
            fp.set_preference( "network.proxy.type", 1 )
            fp.set_preference( "network.proxy.socks_version", 5 )
            fp.set_preference( "network.proxy.socks", '127.0.0.1' )
            fp.set_preference( "network.proxy.socks_port", 9050 )
            fp.set_preference( "network.proxy.socks_remote_dns", True )
            fp.update_preferences()
        else:

        # Direct = 0, Manual = 1, PAC = 2, AUTODETECT = 4, SYSTEM = 5
            fp.set_preference("network.proxy.type",1)
            fp.set_preference("network.proxy.http",proxy.host)
            fp.set_preference("network.proxy.http_port",int(proxy.port))
            fp.set_preference("network.proxy.ssl",proxy.host)
            fp.set_preference("network.proxy.ssl_port",int(proxy.port))
        

    fp.update_preferences()
    
    #
    # Caps : Dictionnary of desired capabilities
    caps = DesiredCapabilities.FIREFOX 
     
    # options: Instance of options.Options
    # 
    # firefox_options : Deprecated argument for options

    #install_addon : usage driver.install_addon('/path/to/addon.xpi')
    #of add_extension: usage fp.add_extension('/path/to/addon.xpi')
    return webdriver.Firefox(firefox_profile=fp, capabilities=caps) 

