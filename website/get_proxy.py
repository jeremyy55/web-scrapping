from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from bs4 import BeautifulSoup as BS
from time import sleep
import random
import logging
from collections import Counter
from selenium.common.exceptions import NoSuchElementException
from website.tools import move_and_click
import sys
sys.path.append('/home/namah/Documents/Ma2/Q2/Tfe/Current/Test/SeleniumTools/')
from model import Proxy

def my_proxy(PROXY_HOST,PROXY_PORT):
    fp = webdriver.FirefoxProfile()
    # Direct = 0, Manual = 1, PAC = 2, AUTODETECT = 4, SYSTEM = 5
    fp.set_preference("network.proxy.type", 1)
    fp.set_preference("network.proxy.http",PROXY_HOST)
    fp.set_preference("network.proxy.http_port",int(PROXY_PORT))
    fp.set_preference("network.proxy.ssl",PROXY_HOST)
    fp.set_preference("network.proxy.ssl_port",int(PROXY_PORT))
    fp.set_preference("general.useragent.override","whater_useragent")
    fp.update_preferences()
    return webdriver.Firefox(firefox_profile=fp)

def get_proxies_on_table(driver,country_name):
    table=driver.find_element_by_class_name('DataGrid')
    #beautifulSoup
    html=table.get_attribute('innerHTML')
    soup=BS(html,'lxml')
    filter_for_high_anonymous= lambda tag : tag.name=='tr' \
        and tag['class'] in [['Odd'],['Even']] \
        and tag.find_all(lambda sub_tag: sub_tag.name == 'td' and sub_tag.text=='High Anonymous')
    tr_for_high_anonymous = soup.find_all(filter_for_high_anonymous)
    saved=[]
    for el in tr_for_high_anonymous:
        tmp=el.find_all('td')
        ip=tmp[0].a.text
        port=tmp[1].text
        #saved.append((ip,port))
        saved.append(Proxy(host=ip,port=port,country=country_name))
    sleep(5)
    return saved

def get_proxy_by_country(driver,country_name):
    menu=driver.find_element_by_id('menu')
    move_and_click(driver,menu,on_element=False)
    sleep(3)
    table=driver.find_element_by_class_name('DataGrid')
    country_element=table.find_element_by_link_text(country_name)
    move_and_click(driver,country_element,on_element=False,scroll=True)
    sleep(3)
    saved=get_proxies_on_table(driver,country_name)

    try:
        while(driver.find_element_by_xpath("//div[@class='page']/a[text()='Next »']")):
            next_button=driver.find_element_by_xpath("//div[@class='page']/a[text()='Next »']")
            move_and_click(driver,next_button,on_element=False,scroll=True)
            sleep(3)
            saved += get_proxies_on_table(driver,country_name)
    except NoSuchElementException:
        print('toutes les pages ont été scrappée')
    logging.info(f"The proxy recovery end up with {len(saved)} potential proxies from {country_name}")
    return saved

def get_working_server(saved,country_name,n_max=4,n_min=2):
    good_one=[]
    total=0
    error_counter=Counter()
    for element in saved:
        total +=1
        try:
            driver=my_proxy(element.host,element.port)
            
            if verification_bis(driver,element.country,element.host):
                good_one.append(element)
                print(f'element append in good one , which len is : {len(good_one)}')
                if len(good_one) ==n_max:
                    return good_one
            else:
                raise Exception('NotWorking')
        except Exception as e:
            print(e)
            exception_name=type(e).__name__
            error_counter[exception_name]+=1
        finally:
            sleep(3)
            driver.close()
        sleep(30)
    print('len(good_one)',len(good_one))
    logging.info(f"Here are the result of selection of proxy:\n{len(good_one)} conserved, {total} used")
    logging.debug(f"Error Analysis:{error_counter}")
    if len(good_one)>=n_min:
        return good_one
    else:
        raise Exception('The function wasn\'t able to get enough proxy server')
        
def verification(driver,country_name,PROXY_HOST):
    driver.get('https://www.whatismyip.org/en/')
    sleep(5)
    el=driver.find_elements_by_class_name('thumbnail')[0]
    move_and_click(driver,el,on_element=False)
    sleep(18)
    el=driver.find_element_by_class_name('table')
    el_innerHTML=el.get_attribute('innerHTML')
    soup=BS(el_innerHTML,'lxml')
    pays=soup.find_all('tr')[3].find_all('td')[1].text
    ip=soup.find_all('tr')[0].find_all('td')[1].text
    return country_name == pays and ip == PROXY_HOST

def verification_bis(driver,country_name,PROXY_HOST):
    driver.get('https://mylocation.org/')
    sleep(5)
    el=driver.find_element_by_id("ui-accordion-accordion-panel-0")
    el_innerHTML=el.get_attribute('innerHTML')
    soup=BS(el_innerHTML,'lxml')
    pays=soup.find('table').find_all('tr')[3].find_all('td')[1].text
    ip=soup.find('table').find_all('tr')[0].find_all('td')[1].text
    #print(f'pays : {pays}, country_name : {country_name}, result: {country_name in pays and ip == PROXY_HOST}')
    return country_name in pays and ip == PROXY_HOST


def get_proxy_from_one_country(country_name,n_max=4):
    #selenium
    url="http://www.freeproxylists.net"
    driver = webdriver.Firefox()
    driver.get(url)
    saved = get_proxy_by_country(driver=driver,country_name=country_name)
    driver.close()

    res = get_working_server(saved=saved,country_name=country_name,n_max=n_max)
    return res

def get_potential_country(driver,n=3):
    menu=driver.find_element_by_id('menu')
    move_and_click(driver,menu,on_element=False)
    sleep(3)
    table=driver.find_element_by_class_name('DataGrid')
    soup=BS(table.get_attribute('innerHTML'),'lxml')
    potr=soup.find_all('tr')[1:]
    filter_for_potential= lambda tag: int(tag.find_all('td')[2].text)>20
    pot= [element.td.a.text for element in potr if filter_for_potential(element) ]
    random.shuffle(pot)
    return pot[:n]
 
def get_proxy_from_multiple_country(random=False,**kwargs):
    url="http://www.freeproxylists.net"
    driver = webdriver.Firefox()
    driver.get(url)
    res =[]
    country_number={}
    if random:
        potential_country=get_potential_country(driver)
        for element in potential_country:
            country_number[element]=3
    else:
        country_number=kwargs


    for key,value in country_number.items():
        try:
            saved = get_proxy_by_country(driver=driver,country_name=key)
            res += get_working_server(saved=saved,country_name=key,n_max=value)
            print(f'res: {res}')
        except NoSuchElementException:
            logging.info('Impossible to find the country {}'.format(key))
        except:
            logging.info(f'not enough proxy server with the country : {key}')
        
    driver.close()
    if len(res)>2:
        return res
    else:
        raise Exception(f'not enough proxy server with {country_number}')

def tor_proxy(host='127.0.0.1',port= '9050'):
    fp = webdriver.FirefoxProfile()
    try:
        fp.set_preference( "network.proxy.type", 1 )
        fp.set_preference( "network.proxy.socks_version", 5 )
        fp.set_preference( "network.proxy.socks", '127.0.0.1' )
        fp.set_preference( "network.proxy.socks_port", 9050 )
        fp.set_preference( "network.proxy.socks_remote_dns", True )
        fp.update_preferences()
        driver= webdriver.Firefox(firefox_profile=fp)
    except:
        logging.warning('An error happened during the creation of the tor driver in tor_proxy')
    driver.get('https://check.torproject.org')
    soup=BS(driver.page_source,'lxml')
    driver.close()
    if soup.find('h1',{'class':'on'}):
        proxy_host=soup.find('strong').text
        return Proxy(country='TOR',host=proxy_host,port=9050)
    elif soup.find('h1',{'class':'off'}):
        print('off')
        raise Exception('YOU ARE NOT USING TOR')
    else:
        raise Exception('Change in the page of https://check.torproject.org')

    



if __name__ == "__main__":
    country_name="Spain"
    #res = get_proxy_from_one_country(country_name,n_max=2)
    kwargs={"Spain":1,'France':1}
    res=get_proxy_from_multiple_country(random=False,**kwargs)
    #saved=get_proxy_by_country(country_name)
    #res=get_working_server(saved,country_name)
