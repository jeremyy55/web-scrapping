""" https://www.kayak.fr/impressum -> comment fonctionne leur système de proposition"""
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from random import uniform
from time import sleep,time

from website.tools import select_and_type, move_and_click

from model import Logs, Worker
from utils import exception_repeated
import logging

r2=exception_repeated(repeated=2)

@r2
def kayak(worker,departure,arrival,begin,end,screenshot=False,refresh=False):
    worker.driver.get('https://kayak.fr')
    sleep(8)
    accept_cookies_usage(worker.driver)
    logging.info('current url: {}'.format(worker.driver.current_url))
    declare_context(worker.driver,departure,begin.slash(),arrival,end.slash())

    sleep(10)
    worker.driver.switch_to.window(worker.driver.window_handles[1])
    consult_result(worker)
    if refresh:
        sleep_sequence=[60,600,60]
        for sleep_delay in sleep_sequence:
            sleep(sleep_delay)
            worker.driver.refresh()
            sleep(2)
            consult_result(worker)

def declare_context(driver,departure,begin,arrival,end,screenshot=False):
    element_departure=driver.find_element_by_css_selector("div[aria-label='Saisie de l’aéroport d’origine']")
    select_and_type(driver,element_departure,departure)
    element_arrival=driver.find_element_by_css_selector("div[aria-label='Saisie de l’aéroport de destination'] ")
    select_and_type(driver,element_arrival,arrival)
    
    element_begin=driver.find_element_by_css_selector("div[aria-label='Sélection de la date aller'] ")
    select_and_type(driver,element_begin, begin)
    element_end=driver.find_element_by_class_name("js-date-end")
    select_and_type(driver,element_end, end)
    element_finish=driver.find_element_by_css_selector('button[aria-label="Trouver un vol"]')
    move_and_click(driver,element_finish,on_element=False)

def accept_cookies_usage(driver):

    try:
        ok_element=driver.find_element_by_css_selector("button[id$='soundsGood']")
        move_and_click(driver,ok_element)
    except Exception as e:
        print(e)

    try:
        ok_element=driver.find_element_by_css_selector("button[aria-label='Accepter']")
        move_and_click(driver,ok_element)
    except Exception as e:
        print(e)

def close_free_alert(driver):
    actions=ActionChains(driver)
    actions.move_by_offset(uniform(5,120),uniform(2,500)).click().perform()

def consult_result(worker,screenshot=False):
    opening_time=time()
    #worker.driver.find_element_by_class_name('Common-Results-ProgressBar')
    while( (time()-opening_time <20) and ('Hidden' not in worker.driver.find_element_by_class_name('Common-Results-ProgressBar').get_attribute('class')) ):
        sleep(2)
        print('sleep')      
    print('WAKE UP !')
    try:
        close_free_alert(worker.driver)
    except Exception as e:
        print(e)

    html=worker.driver.page_source
    worker.logs.append(Logs(url=worker.driver.current_url,html=html))
    worker.session.add(worker)
    worker.session.commit()
    sleep(10)
    
if __name__ == '__main__':
    print('ça ne marche pas mec, lance work sans arguments avec kayak comme task et ça ira ')