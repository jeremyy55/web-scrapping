from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys
from random import uniform
from time import sleep


def select_and_type(driver,element,text_to_type):
    actions = ActionChains(driver)
    actions.move_to_element(element)
    actions.pause(uniform(2,3))
    actions.click(element)
    actions.pause(uniform(1,2))
    actions.send_keys(text_to_type)
    actions.pause(uniform(1,2))
    actions.send_keys(Keys.ENTER)
    actions.perform()

def move_and_click(driver,element,on_element=True,scroll=False):
    if scroll:
        driver.execute_script("arguments[0].scrollIntoView(true);",element)
        sleep(uniform(1,2))
    actions= ActionChains(driver)
    actions.move_to_element(element)
    actions.pause(uniform(2,3))
    if on_element:
        actions.click(element)
    else:
        actions.click()
    actions.perform()