import logging
from website.tools import move_and_click,select_and_type
from time import sleep,time
from selenium.common.exceptions import NoSuchElementException

from model import Worker,Logs

def kiwi(worker,departure,begin,arrival,end,refresh=False):
    worker.driver.get('https://www.kiwi.com/fr')
    sleep(3)
    logging.info('current url: {}'.format(worker.driver.current_url))
    declare_context(worker.driver,departure,begin,arrival,end)
    sleep(2)
    consult_result(worker,screenshot=False)

def declare_context(driver,departure,begin,arrival,end,screenshot=False):
    to_close=driver.find_element_by_class_name('PlacePickerInputPlace-close')
    move_and_click(driver,to_close)
    input_elements=driver.find_elements_by_class_name('SearchField-input')
    select_and_type(driver,input_elements[0],departure)
    select_and_type(driver,input_elements[1],arrival)
    select_date(driver,input_elements[2],begin,end)
    search=driver.find_element_by_css_selector("button[class^='Button__StyledButton']")
    move_and_click(driver,search)
def select_date(driver,element,begin,end):
    move_and_click(driver,element)
    date_tab=driver.find_element_by_class_name('datePicker')
    while True:
        title_div=date_tab.find_elements_by_css_selector("div[class^='styles__MonthContainer']")
        if title_div[0].text ==begin.my():
            good_container=date_tab.find_element_by_css_selector("div[class^='styles__Container']")
            days_container=good_container.find_elements_by_css_selector("div[class^='styles__DayContainer']")
            move_and_click(driver,days_container[(begin.number-1)])
            move_and_click(driver,days_container[(end.number-1)])
            break
        else:
            move_next=date_tab.find_element_by_css_selector("div[data-test='CalendarMoveNext']")
            move_and_click(driver,move_next)
    search_done=date_tab.find_element_by_css_selector("button[data-test='SearchFormDoneButton'")
    move_and_click(driver,search_done)
def consult_result(worker,screenshot=False):
    sleep(2)
    comments=["SortBy-quality","SortBy-price","SortBy-duration"]
    opening_time=time()

    for i in range(len(comments)):
        sleep(3)
        need_to_wait_loading=True
        new_sorting=worker.driver.find_element_by_css_selector("div[data-test='NewSortingButton']")
        move_and_click(worker.driver,new_sorting)
        next_sorting=worker.driver.find_element_by_css_selector(f"tr[data-test='{comments[i]}']")
        move_and_click(worker.driver,next_sorting)

        while need_to_wait_loading:
            sleep(5)
            try:
                
                worker.driver.find_element_by_class_name('LoadingProviders')
                sleep(2)
                print('sleep')
            except NoSuchElementException:
                need_to_wait_loading=False
            except Exception:
                logging.exception('Error in kiwi')
                raise
        
        html=worker.driver.page_source
        url=worker.driver.current_url
        print('la c suppoz et bo')
        sleep(5)
        worker.logs.append(Logs(url=url,html=html,comment=comments[i]))
    worker.session.add(worker)
    worker.session.commit()
    