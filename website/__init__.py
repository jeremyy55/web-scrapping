"""
fichier relatif à la doc du module scrapper:
Dossier visant à contenir les fonctions utiles au scrapper ainsi que les routines pour chaque site consulté.
# here is the webdriver api doc : https://selenium-python.readthedocs.io/api.html
# know more about selenium with python: https://github.com/SeleniumHQ/selenium/tree/master/py
# about the webdriver.selenium options : https://seleniumhq.github.io/selenium/docs/api/py/webdriver_firefox/selenium.webdriver.firefox.options.html
#about the CSS selectors: https://saucelabs.com/resources/articles/selenium-tips-css-selectors

"""