from selenium import webdriver
from collections import Counter
from time import sleep
import logging
from ..tools import move_and_click
from bs4 import BeautifulSoup as BS
from model import Proxy

def my_proxy(PROXY_HOST,PROXY_PORT):
    fp = webdriver.FirefoxProfile()
    # Direct = 0, Manual = 1, PAC = 2, AUTODETECT = 4, SYSTEM = 5
    fp.set_preference("network.proxy.type", 1)
    fp.set_preference("network.proxy.http",PROXY_HOST)
    fp.set_preference("network.proxy.http_port",int(PROXY_PORT))
    fp.set_preference("network.proxy.ssl",PROXY_HOST)
    fp.set_preference("network.proxy.ssl_port",int(PROXY_PORT))
    fp.set_preference("general.useragent.override","whater_useragent")
    fp.update_preferences()
    return webdriver.Firefox(firefox_profile=fp)

def get_working_server(saved,country_name,n_max=4,n_min=2):
    good_one=[]
    total=0
    error_counter=Counter()
    for element in saved:
        total +=1
        try:
            driver=my_proxy(element.host,element.port)
            
            if verification_bis(driver,element.country,element.host):
                good_one.append(element)
                logging.info(f'element append in good one , which len is : {len(good_one)}')
                if len(good_one) ==n_max:
                    return good_one
            else:
                raise Exception('NotWorking')
        except Exception as e:
            #print(e)
            exception_name=type(e).__name__
            error_counter[exception_name]+=1
        finally:
            sleep(3)
            driver.close()
        sleep(30)
    logging.info(f'len(good_one): {len(good_one)}')
    logging.info(f"Here are the result of selection of proxy:\n{len(good_one)} conserved, {total} used")
    logging.debug(f"Error Analysis:{error_counter}")
    if len(good_one)>=n_min:
        return good_one
    else:
        raise Exception('The function wasn\'t able to get enough proxy server')

def verification(driver,country_name,PROXY_HOST):
    driver.get('https://www.whatismyip.org/en/')
    sleep(5)
    el=driver.find_elements_by_class_name('thumbnail')[0]
    move_and_click(driver,el,on_element=False)
    sleep(18)
    el=driver.find_element_by_class_name('table')
    el_innerHTML=el.get_attribute('innerHTML')
    soup=BS(el_innerHTML,'lxml')
    pays=soup.find_all('tr')[3].find_all('td')[1].text
    ip=soup.find_all('tr')[0].find_all('td')[1].text
    return country_name == pays and ip == PROXY_HOST

def verification_bis(driver,country_name,PROXY_HOST):
    driver.get('https://mylocation.org/')
    sleep(5)
    el=driver.find_element_by_id("ui-accordion-accordion-panel-0")
    el_innerHTML=el.get_attribute('innerHTML')
    soup=BS(el_innerHTML,'lxml')
    pays=soup.find('table').find_all('tr')[3].find_all('td')[1].text
    ip=soup.find('table').find_all('tr')[0].find_all('td')[1].text
    #print(f'pays : {pays}, country_name : {country_name}, result: {country_name in pays and ip == PROXY_HOST}')
    return country_name in pays and ip == PROXY_HOST

def tested_tor_proxy(driver):
    driver.get('https://check.torproject.org')
    soup=BS(driver.page_source,'lxml')
    driver.close()
    if soup.find('h1',{'class':'on'}):
        proxy_host=soup.find('strong').text
        return Proxy(country='TOR',host=proxy_host,port=9050)
    elif soup.find('h1',{'class':'off'}):
        raise Exception('YOU ARE NOT USING TOR')
    else:
        raise Exception('Change in the page of https://check.torproject.org')