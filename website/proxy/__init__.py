from selenium import webdriver
import logging
from selenium.common.exceptions import NoSuchElementException
from .test_proxy import get_working_server,tested_tor_proxy
from .get_proxy import get_proxy_by_country,get_potential_country

def get_proxy_from_one_country(*countryname_args,n_max=4):
    #selenium
    url="http://www.freeproxylists.net"
    driver = webdriver.Firefox()
    driver.get(url)
    res=None
    for country_name in countryname_args:
        try:
            saved = get_proxy_by_country(driver=driver,country_name=country_name)
            res = get_working_server(saved=saved,country_name=country_name,n_max=n_max)
        except:
            logging.info(f'not enough proxy server with: {country_name}')
    driver.close()
    if res:
        return res
    else:
        raise Exception('Impossible to find a proxy server with the given list')

def get_proxy_from_multiple_country(random=0,country_min=2,**kwargs):
    assert random+len(kwargs) > country_min, 'you ask more country_min than you gave as input( kwargs and random accumulated)'
    url="http://www.freeproxylists.net"
    driver = webdriver.Firefox()
    driver.get(url)
    res =[]
    country_number={}
    if random:
        potential_country=get_potential_country(driver,n=random)
        for element in potential_country:
            country_number[element]=3
    country_number= {**country_number, **kwargs}
    proxy_country_counter=0
    for key,value in country_number.items():
        try:
            saved = get_proxy_by_country(driver=driver,country_name=key)
            res += get_working_server(saved=saved,country_name=key,n_max=value)
            print(f'res: {res}')
            proxy_country_counter+=1
            if proxy_country_counter >= country_min:
                break
        except NoSuchElementException:
            logging.info('Impossible to find the country {}'.format(key))
        except:
            logging.info(f'not enough proxy server with the country : {key}')
    driver.close()
    if proxy_country_counter >= country_min:
        return res
    else:
        raise Exception(f'not enough proxy server with {country_number}')

def tor_proxy(host='127.0.0.1',port= '9050'):
    fp = webdriver.FirefoxProfile()
    try:
        fp.set_preference( "network.proxy.type", 1 )
        fp.set_preference( "network.proxy.socks_version", 5 )
        fp.set_preference( "network.proxy.socks", '127.0.0.1' )
        fp.set_preference( "network.proxy.socks_port", 9050 )
        fp.set_preference( "network.proxy.socks_remote_dns", True )
        fp.update_preferences()
        driver= webdriver.Firefox(firefox_profile=fp)
    except:
        logging.warning('An error happenedbible during the creation of the tor driver in tor_proxy')
    return tested_tor_proxy(driver)

if __name__ == "__main__":
    country_name=["Spain",'United Kingdom',"France"]
    
    res = get_proxy_from_one_country(*country_name,n_max=2)
    #kwargs={"Spain":1,'France':1}
    #res=get_proxy_from_multiple_country(random=False,**kwargs)
    #saved=get_proxy_by_country(country_name)
    #res=get_working_server(saved,country_name)
