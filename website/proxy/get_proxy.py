from time import sleep
from model import Proxy
from ..tools import move_and_click
from bs4 import BeautifulSoup as BS
import random
from selenium.common.exceptions import NoSuchElementException
import logging

def get_proxies_on_table(driver,country_name):
    table=driver.find_element_by_class_name('DataGrid')
    #beautifulSoup
    html=table.get_attribute('innerHTML')
    soup=BS(html,'lxml')
    filter_for_high_anonymous= lambda tag : tag.name=='tr' \
        and tag['class'] in [['Odd'],['Even']] \
        and tag.find_all(lambda sub_tag: sub_tag.name == 'td' and sub_tag.text=='High Anonymous')
    tr_for_high_anonymous = soup.find_all(filter_for_high_anonymous)
    saved=[]
    for el in tr_for_high_anonymous:
        tmp=el.find_all('td')
        ip=tmp[0].a.text
        port=tmp[1].text
        #saved.append((ip,port))
        saved.append(Proxy(host=ip,port=port,country=country_name))
    sleep(5)
    return saved

def get_proxy_by_country(driver,country_name):
    menu=driver.find_element_by_id('menu')
    move_and_click(driver,menu,on_element=False)
    sleep(3)
    table=driver.find_element_by_class_name('DataGrid')
    country_element=table.find_element_by_link_text(country_name)
    move_and_click(driver,country_element,on_element=False,scroll=True)
    sleep(3)
    saved=get_proxies_on_table(driver,country_name)

    try:
        while(driver.find_element_by_xpath("//div[@class='page']/a[text()='Next »']")):
            next_button=driver.find_element_by_xpath("//div[@class='page']/a[text()='Next »']")
            move_and_click(driver,next_button,on_element=False,scroll=True)
            sleep(3)
            saved += get_proxies_on_table(driver,country_name)
    except NoSuchElementException:
        logging.info('toutes les pages ont été scrappée')
    logging.info(f"The proxy recovery end up with {len(saved)} potential proxies from {country_name}")
    return saved

def get_potential_country(driver,n=3):
    menu=driver.find_element_by_id('menu')
    move_and_click(driver,menu,on_element=False)
    sleep(3)
    table=driver.find_element_by_class_name('DataGrid')
    soup=BS(table.get_attribute('innerHTML'),'lxml')
    potr=soup.find_all('tr')[1:]
    filter_for_potential= lambda tag: int(tag.find_all('td')[2].text)>20
    pot= [element.td.a.text for element in potr if filter_for_potential(element) ]
    random.shuffle(pot)
    return pot[:n]