from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.select import Select
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from random import uniform
from time import sleep, time
from os.path import join

from website.tools import select_and_type,move_and_click
import config
from model import Logs,Worker
from utils import exception_repeated
import logging

r2=exception_repeated(repeated=2)

@r2
def skyscanner(worker,departure,begin,arrival,end,refresh=False):
    worker.driver.get('https://www.skyscanner.fr/')
    sleep(3)
    logging.info('current url: {}'.format(worker.driver.current_url))
    declare_context(worker.driver,departure,begin,arrival,end)
    logging.info('current url: {}'.format(worker.driver.current_url))

    sleep(10)
    compare_proposal(worker,refresh=refresh)
    sleep(5)
    
def select_date(driver,element,strDate):
    move_and_click(driver,element)
    s1 = Select(driver.find_element_by_name('months'))
    s1.select_by_visible_text(strDate.my())
    day_el=driver.find_element_by_css_selector(" button[aria-label='"+strDate.dnmy()+"']")
    move_and_click(driver,day_el)

def declare_context(driver,departure,begin,arrival,end,screenshot=False):
    el_from = driver.find_element_by_id('fsc-origin-search')
    el_to = driver.find_element_by_id('fsc-destination-search')
    select_and_type(driver,el_from,departure)
    select_and_type(driver,el_to,arrival)

    depart_el= driver.find_element_by_id('depart-fsc-datepicker-button')
    depart_date=begin
    select_date(driver,depart_el,depart_date)

    return_el=driver.find_element_by_id('return-fsc-datepicker-button')
    return_date=end
    select_date(driver,return_el,return_date)

    submit=driver.find_element_by_css_selector('button[type="submit"]')
    move_and_click(driver,submit)

def consult_result(worker,screenshot=False):
    opening_time=time()
    while( (time()-opening_time <20) and (worker.driver.find_element_by_css_selector('div.day-list-progress').value_of_css_property('width')!= "100%" ) ):
        sleep(2)
        print('sleep')      
    print('WAKE UP !')  
    sleep(3)
    #
    # Partie sur laquelle travailler
    # #
    #by default the cheapest is presented
    comments=['best','cheapest','fastest']
    data_tab=['fqsscore','price','duration']
    for i in range(len(comments)):
        bcf_element=worker.driver.find_element_by_xpath('//table[@class="fqs-opts"]').find_element_by_xpath(f'//td[@data-tab="{data_tab[i]}"]')
        move_and_click(worker.driver,bcf_element,on_element=False,scroll=True)
        sleep(3)
        
        more_result_button=worker.driver.find_element_by_xpath('//button[@type="button"][text()="Plus de résultats"]')
        try:
            move_and_click(worker.driver,more_result_button,scroll=True)
        except:
            print('rafistolage')
        sleep(3)
        html=worker.driver.page_source
        if screenshot:
            pictures_info=worker.driver.find_element_by_css_selector('p.places-R9WxN').text
            worker.driver.save_screenshot(join(config.PICTURES_DIR,pictures_info+'.png'))
        worker.logs.append(Logs(url=worker.driver.current_url,html=html,comment=comments[i]))
        sleep(3)

    worker.session.add(worker)
    worker.session.commit()

def compare_proposal(worker,refresh=False,screenshot=False):
    count=0
    if worker.driver.find_elements_by_class_name('browse-list-result'):
        for element in worker.driver.find_elements_by_class_name('browse-list-result'):
            count +=1

            #new windows
            current_page=worker.driver.current_window_handle
            el=element.find_element_by_css_selector('li.browse-list-result a')
            new_page_url=el.get_attribute('href')

            worker.driver.execute_script("window.open('"+new_page_url+"')")
            WebDriverWait(worker.driver,10).until(EC.number_of_windows_to_be(2))
            all_pages=worker.driver.window_handles
            new_page=[page for page in all_pages if page != current_page]
            worker.driver.switch_to.window(new_page[0])
            sleep(5)
            print(count,') in the new, look the title :\n ', worker.driver.title)

            consult_result(worker,screenshot=screenshot)
            worker.driver.close()
            worker.driver.switch_to.window(current_page)
    else:
        consult_result(worker,screenshot=screenshot)
        if refresh:
            sleep_sequence=[60,600,60]
            for sleep_delay in sleep_sequence:

                sleep(sleep_delay)
                worker.driver.refresh()
                sleep(2)
                consult_result(worker,screenshot=screenshot)

