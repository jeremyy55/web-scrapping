from sqlalchemy import Column, PrimaryKeyConstraint, UniqueConstraint,ForeignKey, Integer,String,DateTime
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine
from sqlalchemy.orm import relationship,scoped_session,sessionmaker
from sqlalchemy.sql import func
import atexit
import pickle
import os

from config import SQLALCHEMY_DATABASE_URI
from profiles import build_driver
import config

Base = declarative_base()
engine=create_engine(config.SQLALCHEMY_DATABASE_URI)
session_factory = sessionmaker(bind=engine)


#define class and function

class Worker(Base):
    __tablename__='worker'
    id = Column(Integer,primary_key=True)
    profile=Column(String(100))
    useragent=Column(String(100))
    create_at=Column(DateTime,server_default=func.now())

    proxy_id=Column(Integer,ForeignKey('proxy.id'))
    trip_id=Column(Integer,ForeignKey('trip.id'),nullable=False)
    

    def __init__(self,trip_id,useragent=None,profile=None,proxy=None):
        """ """
        super().__init__(trip_id=trip_id,useragent=useragent,profile=profile)
        self.driver = build_driver(profile=profile,useragent=useragent,proxy=proxy) 
        if profile:
            path_pickle=config.PATH_TO_PICKLE+f"{ profile}_cookies.pkl"
            if os.path.exists(path_pickle):
                with open(path_pickle,'rb') as f:
                    cookies = pickle.load(f)
                    for cookie in cookies:
                        self.driver.add_cookie(cookie)
            
            cookies=pickle.load(open(path_pickle,'rb'))
            for cook in cookies:
                self.driver.add_cookie(cook)
        Session=scoped_session(session_factory) # in or out?
        self.session=Session()
        self.proxy=proxy
        atexit.register(self.cleanup)
        
    def __repr__(self):
        return f"<<Worker id : {self.id}, trip_id: {self.trip_id}, profile: {self.profile}, useragent: {self.useragent}, create_at: {self.create_at} and it's proxy: \n {self.proxy}>>\n" 
    
    def cleanup(self):
        print(' Running cleanup')
        if self.profile:
            pickle_path=config.PATH_TO_PICKLE+f"{ self.profile}_cookies.pkl"
            pickle.dump(self.driver.get_cookies(), open(pickle_path,'rb') )
        
        self.driver.close()

class Logs(Base):
    __tablename__='logs'
    id=Column(Integer,primary_key=True)
    url=Column(String(200),nullable=False)
    html=Column(String(),nullable=False)
    timeStamp=Column(DateTime,server_default=func.now())
    worker_id=Column(Integer,ForeignKey('worker.id'))
    comment=Column(String,nullable=True)
    worker = relationship("Worker",backref="logs")
    
class Proxy(Base):
    __tablename__='proxy'
    id=Column(Integer,primary_key=True)
    host=Column(String(50),nullable=False)
    port=Column(Integer,nullable=False)
    country=Column(String,nullable=False)
    worker=relationship('Worker',backref='proxy',uselist=False)

    def __repr__(self):
        return f"id:{self.id}, host:{self.host}, port:{self.port}, country:{self.country}"

class Trip(Base):
    __tablename__='trip'
    id=Column(Integer,primary_key=True)
    city_from=Column(String,nullable=False)
    city_to=Column(String,nullable=False)
    date_from=Column(DateTime,nullable=False)
    date_to=Column(DateTime,nullable=False)
    def __repr__(self):
        return f"from {self.city_from} to {self.city_to}, from {self.date_from} to {self.date_to}"

def init_db():
    """ function which create the database if it not exist"""
    engine=create_engine(SQLALCHEMY_DATABASE_URI)
    Base.metadata.create_all(engine)

#will be execute each time it is called

init_db()



    
