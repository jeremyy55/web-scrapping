import threading
import logging

def get_thread(func):
    def extractor(*args,**kwargs):
        my_thread=threading.Thread(target=func,args=args,kwargs=kwargs)
        return my_thread
    return extractor

def exception_repeated(repeated=0):
    def exception(func):
        """
            create a decorator that wraps the function, logs it's name and exception if it occurs
            inspired by : source for inspiration: https://www.blog.pythonlibrary.org/2016/06/09/python-how-to-create-an-exception-logging-decorator/
        """
        logging.debug(' ## function: %s',func.__name__)
        def wrapper(*args,**kwargs):
            attempts=0
            todo=True
            while todo:
                try:
                    print(f'attempts: {attempts}\n\n')
                    todo=False
                    return func(*args,**kwargs)
                except Exception as e:
                    logging.warning(f'Error while attempts n°{attempts}')
                    logging.error(e,exc_info=True)
                    attempts +=1
                    if attempts >repeated:
                        raise
                    else:
                        todo=True
                    
                    #re-raise the exception
        return wrapper
    return exception


