from dataclasses import dataclass,field
from datetime import date

@dataclass
class StrDate:
    day: str =field(init=False)
    number:int = field(init=True)
    month: str= field(default='septembre')
    number_month:str = field(default='09')
    year:str = field(default='2019')
    days=['samedi','dimanche','lundi','mardi','mercredi','jeudi','vendredi']


    def my(self) -> str:
        return self.month+' '+self.year

    def dnmy(self) -> str:
        return self.day+' '+str(self.number)+' '+self.month+' '+self.year
    def slash(self)->str:
        return str(self.number)+'/'+self.number_month+'/'+self.year[-2:]

    def to_date(self) ->date:
        return date(int(self.year),int(self.number_month),self.number)

    @classmethod
    def from_dnmy(cls,dnmy):
        text=dnmy.split(' ')
        return cls(text[0],text[1],text[2],text[3])
    
    def __repr__(self):
        return f"{self.day},le {self.slash()}"
    
    
    
    def __post_init__(self):
        self.day=self.days[self.number%7]

