from selenium import webdriver 
from pyvirtualdisplay import Display

def build_driver(profile=None,useragent=None,proxy=None):
    fp = webdriver.FirefoxProfile()
    
    fp.set_preference( "network.proxy.type", 1 )
    fp.set_preference( "network.proxy.socks_version", 5 )
    fp.set_preference( "network.proxy.socks", '127.0.0.1' )
    fp.set_preference( "network.proxy.socks_port", 9050 )
    fp.set_preference( "network.proxy.socks_remote_dns", True )
    fp.update_preferences()
    return webdriver.Firefox(firefox_profile=fp) 

if __name__ == "__main__":
    display=Display(visible=0, size=(1920,1080))
    display.start()
    driver=build_driver()
    driver.get('https://check.torproject.org')
    print(driver.page_source)
    display.stop()