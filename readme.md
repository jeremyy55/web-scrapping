<h1> web scrapping: investigation of  webTracking Tracking and Discrimination<h1>

<h2> Info Section:</h2>
<h3> how did i set up the server:</h3>

1. I Install Anaconda and i created an environment in which I directly install pip
    * wget the file for your distribution from Anaconda official website
    * you will probably need to change mod for executing the file : chmod u='xrw' YourAnacondaFile.sh 
    * default installation should be enough
    * after reboot the machine, create an env, called Experiences for more ease of configuration: conda create --name Experiences
    * activate the env : conda activate Experiences # it can be written directly in shell rc file like .bashrc
    * install pip: conda install pip

2. I installed somes librairies:  
    * cf requirements.txt for libraries : pip install -r requirements.txt
    * some can cause troubles while installation 


3. And some softwares:  
    * sudo apt-get install firefox  
4. I installed gecko driver and place it into /sr/local/bin:  
    * wget https://github.com/mozilla/geckodriver/releases/download/v0.24.0/geckodriver-v0.24.0-linux64.tar.gz 
    * tar -xzf geckodriver*.tar.gz  
    * mv geckodriver /usr/local/bin/ # may need sudo  
5. Then i installed a virtual framebuffer  
    * sudo apt-get install xvfb  
    * pip install xvfbwrapper  


<h3>file related to the tool.</h3>

 * here is the webdriver api doc : https://selenium-python.readthedocs.io/api.html 
 * know more about selenium with python: https://github.com/SeleniumHQ/selenium/tree/master/py
 * about the webdriver.selenium options : https://seleniumhq.github.io/selenium/docs/api/py/webdriver_firefox/selenium.webdriver.firefox.options.html
 * about the CSS selectors: https://saucelabs.com/resources/articles/selenium-tips-css-selectors
 * about multi_threads: https://www.tutorialspoint.com/python3/python_multithreading.htm
