import argparse
from website.kayak import kayak
from website.kiwi import kiwi
from website.skyscanner import skyscanner
import random
from utils.strDate import StrDate
from model import Worker

if __name__ == "__main__":
    parser= argparse.ArgumentParser()
    parser.add_argument('task',help='type the website that will be visited during this checking routine',choices=['skyscanner','kayak','kiwi'])
    args=parser.parse_args()

    if args.task=='skyscanner':
        func=skyscanner
    elif args.task=='kayak':
        func=kayak
    elif args.task=='kiwi':
        func=kiwi
    cities=['Barcelone','Madrid',#Spain
    'Paris','Nantes','Lyon',#France
    'Londres','Manchester','Liverpool'#United Kingdom
    ]
    
    departure=random.choice(cities)
    cities.remove(departure)
    arrival = random.choice(cities)
    begin=random.randint(1,23)# limit are include
    end=begin+7
    begin=StrDate(begin)
    end=StrDate(end)
    refresh=False
    kwargs={'departure':departure,'arrival':arrival,'begin':begin,'end':end,'refresh':refresh}
    kwargs['worker']=Worker(trip_id=666)
    func(**kwargs)
    print('fin fin fin')
