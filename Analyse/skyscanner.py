from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker,scoped_session
from bs4 import BeautifulSoup 
import re

from matplotlib.dates import date2num, DateFormatter
import matplotlib.pyplot as plt

import traceback

# My own library
from Analytics import jaccard_index,kendall_tau_b_coefficient
from SharedClasses import Context, Journey,RoundTrip
import os


import sys
sys.path.append('/home/namah/Documents/Ma2/Q2/Tfe/Current/Test/SeleniumTools/')
from model import Worker,Logs,Proxy,Trip


class Skyscanner():
    def __init__(self,worker,proxy,logs,trip):
        self.context=Context(worker=worker,proxy=proxy,trip=trip)
        self.logs=logs

        self.soups=None
        self.best=None
        self.cheapest=None
        self.fastest=None
        self.sort_by={}
        self._process_logs()
    
    def _process_logs(self):
        self._create_soups()
        self._extract_bcf()
        self._fill_sort_by()

    def _create_soups(self):
        self.soups=[]
        for log in self.logs:
            self.soups.append(BeautifulSoup(log.html,'lxml'))
        
    def _extract_bcf(self):
        bests=[]
        cheapests=[]
        fastests=[]
        get_by_category = lambda table,data_tab_name :int(re.sub('[^0-9]','',table.find('td',{'data-tab':data_tab_name}).find('span',{'class':'fqs-price'}).text))
        for soup in self.soups:
            table=soup.find("table",{"class":"fqs-opts"})
            bests.append(get_by_category(table,'fqsscore'))
            cheapests.append(get_by_category(table,'price'))
            fastests.append(get_by_category(table,'duration'))
        if(len(set(bests)) <=1 and len(set(cheapests)) <=1 and len(set(fastests)) <=1 ):
            self.best=bests[0]
            self.cheapest=cheapests[0]
            self.fastest=fastests[0]
        else:
            raise Exception(f"""All the logs contains difference for the cheapest,best and fastests:
            bests:{bests}
            cheapests:{cheapests}
            fastests:{fastests}""")
    def _fill_sort_by(self):
        
        for soup in self.soups:
            day_list_container=soup.find('div',{'class':"day-list-container"})
            day_list_items=day_list_container.select('li[class^="day-list-item ItinerariesContainer__dayListItem"]')#avoid advertising
            #print(len(day_list_items)) #longueur de la liste de résultat en dessous 
            key_name=soup.find('td',{'class':'active'})['data-tab']
            self.sort_by[key_name]=[]
            if not day_list_items:
                raise Exception('No day_list_item above the best, cheapest and fastest')
            else:
                #print('len list items: ',len(day_list_items))
                for day_list_item in day_list_items:
                    round_trip=RoundTrip.item_from_skyscanner(day_list_item)
                    if round_trip:
                        self.sort_by[key_name].append(round_trip)
    
    def __repr__(self):
        return f"context: {self.context}"
if __name__ == "__main__":
    #db_file="test_same_country_1305.db"
    db_file="test_same_country_2705_final_version.db"
    SQLite_URI='sqlite:///Db/'+db_file
    engine=create_engine(SQLite_URI)
    session_factory=sessionmaker(engine)
    Session=scoped_session(session_factory)
    session=Session()
    exp_folder='same_country/'
    # txt files
    path_for_result_folder='./results/'
    folder_for_results_of_the_experience=os.path.join(path_for_result_folder,exp_folder)
    if not os.path.exists(folder_for_results_of_the_experience):
        os.mkdir(folder_for_results_of_the_experience)

    #png files
    path_for_result_folder='./photos/'
    path_for_photo_folder=os.path.join(path_for_result_folder,exp_folder)
    if not os.path.exists(path_for_photo_folder):
        os.mkdir(path_for_photo_folder)
    
    #get the results
    trips=session.query(Trip).join(Worker,Worker.trip_id==Trip.id).join(Logs,Logs.worker_id==Worker.id).group_by(Worker.trip_id).filter(Logs.url.like('%skyscanner%')).all()
    by_trips=[]
    for trip in trips:
        by_trip=[]
        trip_id=trip.id
        proxy_and_workers=session.query(Worker,Proxy).join(Proxy,Worker.proxy_id==Proxy.id).filter(Worker.trip_id==trip_id).all()
        for proxy_and_worker in proxy_and_workers:
            worker_id=proxy_and_worker.Worker.id
            logs=session.query(Logs)\
            .join(Worker,Worker.id==Logs.worker_id)\
            .join(Proxy,Worker.proxy_id==Proxy.id)\
            .join(Trip,Trip.id == Worker.trip_id)\
            .filter(Logs.worker_id == worker_id)\
            .order_by(Logs.timeStamp)\
                .all()#tous les logs pour un travailleur
            
            for i in range(0,len(logs),3):
                try:
                    skyscanner=Skyscanner(worker=proxy_and_worker.Worker,proxy=proxy_and_worker.Proxy,trip=trip,logs=logs[i:i+3])
                    by_trip.append(skyscanner)
                except Exception as e:
                    print('error with worker : {} from {}'.format(proxy_and_worker.Worker, proxy_and_worker.Proxy.country))
                    print(f'error: {e}')
                    soup=BeautifulSoup(logs[0].html,'lxml')
                    print(f'soup: {soup.body.prettify()[70000:90000]}')
                    waitKey=input()
                
        by_trips.append(by_trip)

    #Analyze results
    analyze_graph=True
    if analyze_graph:
        for by_trip in by_trips: #pour chacun des voyages
            print("####\n\n\n\n new trip \n########")
            if(len(by_trip)>1):
                filename=f'skyscanner_trip_{by_trip[0].context.trip.id}.txt'
                filepath=os.path.join(folder_for_results_of_the_experience,filename)
                print(filepath)
                with open(filepath,'w+') as f:

                #index
                    for i in range(0,len(by_trip)):
                        f.write('# # # ')
                        f.write(f"skyscanner i={i}: {by_trip[i]}, from {by_trip[i].context.proxy.country}")
                        f.write('# # # \n')
                        for j in range(i+1,len(by_trip)):
                            f.write(f"\nskyscanner j={j}: {by_trip[j]} , from {by_trip[j].context.proxy.country}\n")
                            for key in by_trip[i].sort_by.keys():
                                try:
                                    f.write('# # key:{} # #\n'.format(key))
                                    first=by_trip[i].sort_by[key]
                                    second=by_trip[j].sort_by[key]
                                    jac=jaccard_index(first,second)
                                    f.write(f"jaccard: {jac}\n")    
                                    ktb=kendall_tau_b_coefficient([x.price for x in first],[y.price for y in second])
                                    f.write(f'kendall for price:{ktb}\n')
                                    if jac <0.7 or ktb <0.7:
                                        f.write("# weird value for test:\n")
                                        filen=min(len(first),len(second))

                                        f.write(' # Look for sozies: #\n')
                                        for r in range(filen):
                                            for v in range(r+1,filen):
                                                if first[r].sozie(second[v]):
                                                    f.write(f"SOZIE: \ni:{r} from {by_trip[i].context.proxy.country} and j:{v} from {by_trip[i].context.proxy.country}\nfirst:{first[r]}\n second: {second[v]}\n")

                                except KeyError:
                                    f.write('#####\n\n\n\nERRRROOOOOORRRRR\n\n\n\n')
                                    f.write(f'impossible to find key: {key}')
                                except Exception:
                                    f.write('#####\n\n\n\nERRRROOOOOORRRRR\n\n\n\n')
                                    f.write('problem in analyzis')
                                    traceback.print_exc()
                                
                #graph
                time=[skyscanner.logs[0].timeStamp for skyscanner in by_trip]
                dates=date2num(time)
                cheapest=[skyscanner.cheapest for skyscanner in by_trip]
                best=[skyscanner.best for skyscanner in by_trip]
                fastest=[skyscanner.fastest for skyscanner in by_trip]
                plt.figure()
                plt.plot(dates,cheapest,label='cheap',marker='o')
                plt.plot(dates,best,label='best',marker='o')
                plt.plot(dates,fastest,label='fastest',marker='o')
                plt.legend()
                plt.gcf().autofmt_xdate()
                myFmt= DateFormatter('%d/%m %H:%M:%S')
                plt.gca().xaxis.set_major_formatter(myFmt)
                #plt.show()

                
                name_for_the_photo=f'skyscanner_{by_trip[0].context.trip.id}.png'
                photo_path=os.path.join(path_for_photo_folder,name_for_the_photo)
                plt.savefig(photo_path)