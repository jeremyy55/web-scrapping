import re

class Context():
    def __init__(self,worker,proxy,trip):
        self.worker=worker
        self.proxy=proxy
        self.trip=trip
    def __repr__(self):
        return f"worker.id:{self.worker.id}, proxy.id{self.proxy.id}, trip.id:{self.trip.id}"
class Journey():
    def __init__(self,company,departure,arrival):
        self.company=company
        self.departure=departure
        self.arrival=arrival
    def __repr__(self):
        return f"Journey from {self.departure} to {self.arrival} with {self.company}"
    def __eq__(self,other):
        return self.company == other.company and self.departure == other.departure and self.arrival == other.arrival
    

    
class RoundTrip():
    def __init__(self,price,go_company,go_departure,go_arrival,back_company,back_departure,back_arrival):
        self.price=price
        self.go=Journey(company=go_company,departure=go_departure,arrival=go_arrival)
        self.back=Journey(company=back_company,departure=back_departure,arrival=back_arrival)
    def __repr__(self):
        return f"go: {self.go}\n and back: {self.back}\n for this price: {self.price}"
    def __eq__(self,other):
        assert self.go == other.go and self.back == other.back
        assert self.price == other.price, f' information are the same but not the price!\nbetween{self} and{other}'
        return self.go == other.go and self.back == other.back and self.price==other.price
    #def __lt__(self,other):
    #    return self.price > other.price
    def __hash__(self):
        return hash(str(self))
    def sozie(self,other):
        return self.go == other.go and self.back == other.back
    
    @classmethod
    def item_from_kayak(cls,item):
        try:
            price=int(re.sub('[^0-9]','',item.find('span',{"class":"price option-text"}).text))
            flights=item.find('ol',{'class':"flights"}).find_all('li')
            flight= flights[0]
            top=flight.find('div',{"class":"top"})
            go_departure=top.find('span',{"class":"depart-time"}).text
            go_arrival=top.find('span',{"class":"arrival-time"}).text
            bottom=flight.find('div',{"class":"bottom"})
            go_company=bottom.text

            flight=flights[1]
            back_departure=top.find('span',{"class":"depart-time"}).text
            back_arrival=top.find('span',{"class":"arrival-time"}).text
            bottom=flight.find('div',{"class":"bottom"})
            back_company=bottom.text

            return cls(price=price,go_departure=go_departure,go_arrival=go_arrival,go_company=go_company,back_arrival=back_arrival,back_departure=back_departure,back_company=back_company)
        except:
            raise Exception('Error in item_from_kayak')
    
    
    @classmethod
    def item_from_skyscanner(cls,item):
        round_trip=item.select('div[class^="Leg__itinerary-leg"]')
        try:
            price=int(re.sub('[^0-9]','', item.find('a',{'data-e2e':"itinerary-price"}).text))            
            if round_trip[0].find('div',{'class':'big-logo'}):# une seule compagnie
                go_company=round_trip[0].find('div',{'class':'big-logo'}).img.get('alt')
                go_departure=round_trip[0].select('div[class^="LegInfo__leg-depart"]')[0].span.text
                go_arrival=round_trip[0].select('div[class^="LegInfo__leg-arrive"]')[0].span.text
            elif round_trip[0].select('span[class^="AirlineLogo"]'):
                go_company=round_trip[0].select('span[class^="AirlineLogo"]')[0].text
                go_departure=round_trip[0].select('div[class^="LegInfo__leg-depart"]')[0].span.text
                go_arrival=round_trip[0].select('div[class^="LegInfo__leg-arrive"]')[0].span.text
            else:
                raise Exception('Case not threat by the class')

            if round_trip[1].find('div',{'class':'big-logo'}):
                back_company=round_trip[1].find('div',{'class':'big-logo'}).img.get('alt')
                back_departure=round_trip[1].select('div[class^="LegInfo__leg-depart"]')[0].span.text
                back_arrival=round_trip[1].select('div[class^="LegInfo__leg-arrive"]')[0].span.text
            elif round_trip[1].select('span[class^="AirlineLogo"]'):
                back_company=round_trip[1].select('span[class^="AirlineLogo"]')[0].text
                back_departure=round_trip[1].select('div[class^="LegInfo__leg-depart"]')[0].span.text
                back_arrival=round_trip[1].select('div[class^="LegInfo__leg-arrive"]')[0].span.text
            else:
                raise Exception('Case not threat by the class')

            return cls(price=price,go_company=go_company,go_departure=go_departure,go_arrival=go_arrival,back_company=back_company,back_departure=back_departure,back_arrival=back_arrival)
        except Exception as e:
            if item.select('div[class^="SponsoredTicket"]'):        
                return None#print('sponsored ticket')
            else:
                print('shit in the item_from_skyscanner\nexcept : ',e)
                print(item.prettify())
                raise 
            