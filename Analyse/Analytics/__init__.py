""" tools which defines differents tools for analyzing list of element"""

import numpy as np

def jaccard_index(list_1,list_2):
    set_1=set(list_1)
    set_2=set(list_2)

    intersection=set_1.intersection(set_2)
    union=set_1.union(set_2)
    return len(intersection)/len(union)

def kendall_tau_a_coefficient(param_1,param_2):
    value=0
    n_1=0
    min_len=min(len(param_1),len(param_2))
    for i in range(1,min_len):
        for j in range(i):
            tmp_sign=np.sign(param_1[i]-param_1[j]) * np.sign(param_2[i]-param_2[j])
            value+= tmp_sign
            n_1 += np.abs(tmp_sign)
    n_0 = 2/(min_len*(min_len-1))
    value *= n_0
    return value

def kendall_tau_b_coefficient(param_1,param_2):
    value=0
    n_1=0
    min_len=min(len(param_1),len(param_2))
    for i in range(1,min_len):
        for j in range(i):
            tmp_sign=np.sign(param_1[i]-param_1[j]) * np.sign(param_2[i]-param_2[j])
            value+= tmp_sign
            n_1 += np.abs(tmp_sign)
    value /= n_1
    return value
    
    