from Analytics import jaccard_index, kendall_tau_a_coefficient,kendall_tau_b_coefficient

def test_jaccard_index():
    a=[1,2,3,4,5,6,7]
    b=[4,5,6,7,8,9,10]
    #intersection: 4,5,6,7 
    #union : 1,2,3,4,5,6,7,8,9,10 
    assert jaccard_index(a,b) == 0.4

def test_kendall_tau():
    a=[1,2,3]
    b=[1,2,3]
    c=[1,1,1]

    #a
    assert kendall_tau_a_coefficient(a,b) ==1
    assert kendall_tau_a_coefficient(a,b[::-1]) == -1
    assert kendall_tau_a_coefficient(a,c)==0
    #b
    assert kendall_tau_b_coefficient(a,b) ==1
    assert kendall_tau_b_coefficient(a,b[::-1]) == -1
    assert kendall_tau_b_coefficient(a,c)==0
    
    



