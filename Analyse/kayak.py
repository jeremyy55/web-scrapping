from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker,scoped_session
from bs4 import BeautifulSoup 

from matplotlib.dates import date2num, DateFormatter
import matplotlib.pyplot as plt

import traceback
import sys
import os

# My own library
from Analytics import jaccard_index,kendall_tau_b_coefficient
from SharedClasses import Context, Journey,RoundTrip

sys.path.append('/home/namah/Documents/Ma2/Q2/Tfe/Current/Test/SeleniumTools/')
from model import Worker,Logs,Proxy,Trip

class Kayak:
    def __init__(self,worker,proxy,log,trip):
        self.context=Context(worker=worker,proxy=proxy,trip=trip)
        self.logs=[log]

        self.soups=None
        self.sort_by={}
        self._process_logs()

    def _process_logs(self):
        self._create_soups()
        self._fill_sort_by()
    def _create_soups(self):
        self.soups=[]
        for log in logs:
            self.soups.append(BeautifulSoup(log.html,'lxml'))
    def _fill_sort_by(self):
        for soup in self.soups:
            search_result_list=soup.find('div',{'id':'searchResultsList'})
            item_list=search_result_list.select("div[class^='Base-Results-HorizonResult']")
            key_name='price'
            self.sort_by[key_name]=[]
            if not item_list:
                raise Exception('No item list , It is not normal')
            else:
                for item in item_list:
                    self.sort_by[key_name].append(RoundTrip.item_from_kayak(item))

    def __repr__(self):
        return f'context: {self.context}'

if __name__ == "__main__":
    db_file="test_same_country_2705_final_version.db"
    SQLite_URI='sqlite:///Db/'+db_file
    engine=create_engine(SQLite_URI)
    session_factory=sessionmaker(engine)
    Session=scoped_session(session_factory)
    session=Session()

    path_for_result_folder='./results/'
    folder_for_results_of_the_experience=os.path.join(path_for_result_folder,'same_country/')
    if not os.path.exists(folder_for_results_of_the_experience):
        os.mkdir(folder_for_results_of_the_experience)
    
    #get the results
    trips=session.query(Trip).join(Worker,Worker.trip_id==Trip.id).join(Logs,Logs.worker_id==Worker.id).group_by(Worker.trip_id).filter(Logs.url.like('%kayak%')).all()
    by_trips=[]
    for trip in trips:
        by_trip=[]
        trip_id=trip.id
        proxy_and_workers=session.query(Worker,Proxy).join(Proxy,Worker.proxy_id==Proxy.id).filter(Worker.trip_id==trip_id).all()
        for proxy_and_worker in proxy_and_workers:
            worker_id=proxy_and_worker.Worker.id
            logs=session.query(Logs)\
            .join(Worker,Worker.id==Logs.worker_id)\
            .join(Proxy,Worker.proxy_id==Proxy.id)\
            .join(Trip,Trip.id == Worker.trip_id)\
            .filter(Logs.worker_id == worker_id)\
            .order_by(Logs.timeStamp)\
                .all()
            for i in range(len(logs)):
                try:
                    kayak=Kayak(worker=proxy_and_worker.Worker,proxy=proxy_and_worker.Proxy,trip=trip,log=logs[i])
                    by_trip.append(kayak)
                except:
                    traceback.print_exc()
            by_trips.append(by_trip)
    
    #Analyze results
    analyze_graph=True
    if analyze_graph:
        for by_trip in by_trips: #pour chacun des voyages
            print("####\n new trip \n########")
            if(len(by_trip)>1):
                filename=f'kayak_trip_{by_trip[0].context.trip.id}.txt'
                filepath=os.path.join(folder_for_results_of_the_experience,filename)
                print(filepath)
                with open(filepath,'w+') as f:
                    
                    #index
                    try:
                        for i in range(0,len(by_trip)):
                            f.write('# # # ')
                            f.write(f"kayak i={i}: {by_trip[i]}, from {by_trip[i].context.proxy.country}")
                            f.write('# # # \n')
                            for j in range(i+1,len(by_trip)):
                                f.write(f"\n kayak j={j}:\n {by_trip[j]} , from {by_trip[j].context.proxy.country}\n")
                                for key in by_trip[i].sort_by.keys():
                                    """ 
                                    Traceback (most recent call last):
                                    File "kayak.py", line 95, in <module>
                                        for key in by_trip[i].sort_by.keys():
                                    IndexError: list index out of range                          
                                    """
                                
                                    f.write(' # # key:{}# #\n'.format(key))
                                    first=by_trip[i].sort_by[key]
                                    second=by_trip[j].sort_by[key]
                                    jac=jaccard_index(first,second)
                                    f.write(f"jaccard: {jac}\n")    
                                    ktb=kendall_tau_b_coefficient([x.price for x in first],[y.price for y in second])
                                    f.write(f'kendall for price:{ktb}\n')
                                    if jac <0.7 or ktb <0.7:
                                        f.write("# weird value for test:\n")
                                        filen=min(len(first),len(second))
                                        #for x in range(filen):
                                        #    print(f'i:{x} \n, first:{first[x]},\n second: {second[x]} \n')
                                        f.write(' # Look for sozies: #\n')
                                        for r in range(filen):
                                            for v in range(r+1,filen):
                                                if first[r].sozie(second[v]):
                                                    """

                                                        Traceback (most recent call last):
                                                        File "kayak.py", line 120, in <module>
                                                            print(f"sozie: \ni:{r} and v:{v}\nfirst:\n{first[r]}\n second:\n{second[v]}")
                                                        IndexError: list index out of range

                                                    """
                                                    f.write(f"SOZIE: \ni:{r} from {by_trip[i].context.proxy.country}and v:{v} from {by_trip[j].context.proxy.country}\nfirst:\n{first[r]}\n second:\n{second[v]}\n")

                    except KeyError:
                        f.write(f'#####\n\n\n\nERRRROOOOOORRRRR\n\n\n\nimpossible to find key: {key}')
                    except Exception:
                        print('#####\n\n\n\nERRRROOOOOORRRRR\n\n\n\nproblem in analyzis with \n')
                        traceback.print_exc()
                

    print('c\'est fini sans embuches')
                            

    
                

