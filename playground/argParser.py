import argparse
parser = argparse.ArgumentParser()
#required arguments
parser.add_argument('square',
                    help='square function as in the example',
                    type=int)

#optional arguments
parser.add_argument("-v",'--verbosity',
            help="explain the math behind square, lol",
            action="store_true")
args=parser.parse_args()

if args.verbosity:
    message=f'{args.square}*{args.square}={args.square**2}'
else:
    message=args.square

print(message)

