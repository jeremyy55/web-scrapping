from selenium import webdriver
from fake_useragent import UserAgent

if __name__ == '__main__':
    ua=UserAgent()
    profile = webdriver.FirefoxProfile()
    ua_txt=ua.msie
    print(ua_txt)
    profile.set_preference("general.useragent.override", ua_txt)
    driver=webdriver.Firefox(profile)
    driver.get('http://useragent.fr/')