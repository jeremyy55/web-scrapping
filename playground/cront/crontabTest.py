from os.path import abspath,dirname,join
from importlib import reload
import logging

import threading

def exception(func):
    """
        create a decorator that wraps the function, logs it's name and exception if it occurs
        inspired by : source for inspiration: https://www.blog.pythonlibrary.org/2016/06/09/python-how-to-create-an-exception-logging-decorator/
    """
    logging.debug(' ## function: %s',func.__name__)
    def wrapper(*args,**kwargs):
        #logger=create_logger()
        try:
            return func(*args,**kwargs)
        except:
            logging.exception("There is an error\n")
            #re-raise the exception
            raise
    return wrapper

@exception
def error_function():
    print('before exception')
    raise Exception()

if __name__ == "__main__":
    

    PROJECT_DIR = abspath(dirname(__file__))
    example_file=join(PROJECT_DIR,'example.txt')
    log_file=join(PROJECT_DIR,'example.logs')
    #
    #define logger
    #

    reload(logging) # needed for creating the log file in anaconda. In other case, It use the anaconda root file
    logging.basicConfig(format='%(asctime)s# %(levelname)s: %(message)s',
                        datefmt='%m/%d/%Y %H:%M:%S',
                        filename=log_file,
                        level=logging.DEBUG)

    th=threading.Thread(target=error_function)
    th.start()
    
    with open(example_file,'a') as f:
        f.write('\nhaha\n')
        logging.info('haha')
        f.write(PROJECT_DIR)

        f.write('\nnew with logs\n')