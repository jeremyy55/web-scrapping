#
# 
# Threading.semaphore : https://stackoverflow.com/questions/31508574/semaphores-on-python
#

from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Integer, Column,String
from sqlalchemy import ForeignKey
from sqlalchemy.orm import relationship, sessionmaker, scoped_session
from time import sleep
from threading import Thread
SQLITE_URI= 'sqlite:///th.db'

Base= declarative_base()
class User(Base):
    __tablename__="user"
    id=Column(Integer,primary_key=True)
    name=Column(String,nullable=False)

class Email(Base):
    __tablename__="email"
    id=Column(Integer,primary_key=True)
    address=Column(String,nullable=False)
    user_id=Column(Integer,ForeignKey('user.id'))
    user=relationship("User",backref="emails")
    def __repr__(self):
        return f'id: {self.id}, email: {self.address}'

def dummies_in(user,n_element=3):
    engine=create_engine(SQLITE_URI)
    session_factory= sessionmaker(bind=engine)
    Session= scoped_session(session_factory)
    session= Session()
    print('in dummies')
    sleep(1)
    for i in range(5):
        print(f'i : {i}')
        for n in range(n_element):
            print(f'user : {user.name}, n : {n}/ {n_element}')
            user.emails.append(Email(address=f"{n}@{user.name}.com"))
            sleep(1)

        print('add of user')
        session.add(user)
        print('commit')
        session.commit()
        sleep(1)
    Session.remove()

def dummies_out(user,session,n_element=3):
    print('out dummies')
    sleep(1)
    for i in range(5):
        print(f'i : {i}')
        for n in range(n_element):
            print(f'user : {user.name}, n : {n}/ {n_element}')
            user.emails.append(Email(address=f"{n}@{user.name}.com"))
            sleep(1)

        print('add of user')
        session.add(user)
        print('commit')
        session.commit()
        sleep(1)
def quer():
    engine=create_engine(SQLITE_URI)
    session_factory= sessionmaker(bind=engine)
    Session= scoped_session(session_factory)
    session= Session()
    for i in range(8):
        print('############\nquerrrrrryyyyyyyy n°',i)
        for i,element in  enumerate(session.query(Email).all()):
            print(f'i: {i}, email: {element}')
        print('end querr')
        sleep(3)
    Session.remove()



if __name__ == "__main__":
    engine=create_engine(SQLITE_URI)
    Base.metadata.create_all(engine)
    session_factory= sessionmaker(bind=engine)
    Session= scoped_session(session_factory)
    session= Session()
    other_session= Session()

    u1=User(name='paul')
    u2=User(name='Matthieu')

    threads=[Thread(target=dummies_in,args=(u1,))]
    threads.append(Thread(target=dummies_out,args=(u2,other_session,)))
    threads.append(Thread(target=quer))

    for thread in threads:
        thread.start()
    for thread in threads:
        thread.join()
    
    print('end of file ///////////////')