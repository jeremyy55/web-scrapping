from selenium import webdriver
from selenium.webdriver.common.proxy import Proxy, ProxyType
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from time import sleep
#http://www.freeproxylists.net/fr/country.html


HOST = '178.213.130.159'
PORT = "41425"
def my_proxy(PROXY_HOST,PROXY_PORT):
    fp = webdriver.FirefoxProfile()
    # Direct = 0, Manual = 1, PAC = 2, AUTODETECT = 4, SYSTEM = 5
    fp.set_preference("network.proxy.type", 1)
    fp.set_preference("network.proxy.http",PROXY_HOST)
    fp.set_preference("network.proxy.http_port",int(PROXY_PORT))
    fp.set_preference("network.proxy.ssl",PROXY_HOST)
    fp.set_preference("network.proxy.ssl_port",int(PROXY_PORT))
    fp.set_preference("general.useragent.override","whater_useragent")
    fp.update_preferences()
    return webdriver.Firefox(firefox_profile=fp)


if __name__ == "__main__":
    

    browser = my_proxy(HOST,PORT)
    browser.get('http://whatismyipaddress.com/')
    sleep(10)
    #browser.get('http://www.iplocation.net/find-ip-address')
    ##https://www.iplocation.net/

    browser.get('https://www.skyscanner.fr')
