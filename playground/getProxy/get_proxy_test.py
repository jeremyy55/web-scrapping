from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from bs4 import BeautifulSoup as BS
from time import sleep

from proxy_test import my_proxy

def get_proxy_by_country(country_name):
    url="http://www.freeproxylists.net"
    driver = webdriver.Firefox()
    driver.get(url)
    menu=driver.find_element_by_id('menu')
    ActionChains(driver).move_to_element(menu)\
        .pause(1.5).click().perform()
    sleep(3)
    table=driver.find_element_by_class_name('DataGrid')
    country_element=table.find_element_by_link_text(country_name)

    driver.execute_script("arguments[0].scrollIntoView(true);",country_element)
    sleep(3)
    ActionChains(driver).move_to_element(country_element)\
        .pause(1.5).click().perform()
    sleep(3)
    table=driver.find_element_by_class_name('DataGrid')
    html=table.get_attribute('innerHTML')
    soup=BS(html,'lxml')
    filter_for_high_anonymous= lambda tag : tag.name=='tr' \
        and tag['class'] in [['Odd'],['Even']] \
        and tag.find_all(lambda sub_tag: sub_tag.name == 'td' and sub_tag.text=='High Anonymous')
    tr_for_high_anonymous = soup.find_all(filter_for_high_anonymous)
    saved=[]
    for el in tr_for_high_anonymous:
        tmp=el.find_all('td')
        upTime=int(tmp[7].text.split('.')[0])
        ip=tmp[0].a.text
        port=tmp[1].text
        saved.append((ip,port,upTime))

    sort_by_third=lambda e: e[-1]
    saved.sort(reverse=True,key=sort_by_third)
    driver.close()
    return saved

def get_working_server(saved,country_name,n=2):
    good_one=[]
    for element in saved:
        try:
            driver=my_proxy(element[0],element[1])
            driver.get('https://www.whatismyip.org/')
            sleep(5)
            el=driver.find_elements_by_class_name('thumbnail')[0]
            #.find_element_by_css_selector("a[class='seotoollink'][data-original-title='My IP Address'")
            ActionChains(driver).move_to_element(el)\
                .pause(1).click().perform()
            sleep(10)
            el=driver.find_element_by_class_name('table')
            el_innerHTML=el.get_attribute('innerHTML')
            if verification(el_innerHTML,country_name,element[0]):
                good_one.append(element)
                if len(good_one) ==n:
                    return good_one
            else:
                raise Exception('NotWorking')
        except Exception as e:
            print(e)
        finally:
            sleep(3)
            driver.close()
        sleep(30)
    return None
        


def verification(html,country_name,PROXY_HOST):
    soup=BS(html,'lxml')
    pays=soup.find_all('tr')[3].find_all('td')[1].text
    ip=soup.find_all('tr')[0].find_all('td')[1].text
    return country_name == pays and ip == PROXY_HOST

    
    


if __name__ == "__main__":
    country_name='France'
    saved=get_proxy_by_country(country_name)
    res=get_working_server(saved,country_name)

    
    
    