import logging

#https://www.blog.pythonlibrary.org/2016/06/09/python-how-to-create-an-exception-logging-decorator/
def create_logger():
    """
        creates a logging object and return it
    """
    logger=logging.getLogger('Working_logger')
    logger.setLevel(logging.DEBUG)

    #create the Logging file handler:
    fh=logging.FileHandler("test.log")
    fmt='%(asctime)s# %(levelname)s: %(message)s'
    formatter = logging.Formatter(fmt)
    fh.setFormatter(formatter)

    logger.addHandler(fh)
    return logger

def exception(func):
    """
        create a decorator that wraps the function, logs it's name and exception if it occurs
    """
    def wrapper(*args,**kwargs):
        #logger=create_logger() # using it 
        try:
            return func(*args,**kwargs)
        except:
            logging.exception("There is an error\n")
            #re-raise the exception
            raise
    return wrapper


@exception
def stupid_func():
    print('log lol')
    a=1/0 # error
    logging.info('a')


    