import _thread
import time
import threading

def stupid_function_for_thread(thread_name,delay):
    for i in range(5):
        time.sleep(delay)
        print(thread_name,' à ', time.ctime(time.time()))
def with_thread():
    """ low level interaction with _threads"""
    try:
        _thread.start_new_thread(stupid_function_for_thread, ("my first thread", 1, ))
        _thread.start_new_thread(stupid_function_for_thread,("second dummy t",2, ))
    except Exception as E:
        print("ça pue mec\n",E)
    
    while 1:
        pass

class ThreadWorker(threading.Thread):
    def __init__(self,threadID,name,delay):
        threading.Thread.__init__(self)
        self.ThreadID= threadID
        self.name = name
        self.delay=delay
    def run(self):
        print('starting thread : '+self.name)
        stupid_function_for_thread(self.name,self.delay)
        print('thread stopping : '+ self.name)

def with_threading():
    #creating the treads:
    thread1=ThreadWorker(1,"Thread-1",1.5)
    thread2=ThreadWorker(2,"thread_2 nigga",3)

    thread1.start()
    thread2.start()
    #join specify that you are waiting for the thread to finish before going further in the code
    thread1.join()
    thread2.join()
    print('End of main thread')


if __name__ == "__main__":
    print("début")
    with_threading()
    
    